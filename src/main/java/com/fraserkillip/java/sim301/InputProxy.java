package com.fraserkillip.java.sim301;

import org.newdawn.slick.Input;
import org.newdawn.slick.KeyListener;
import org.newdawn.slick.MouseListener;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to tunnel input from the root game to listeners because Slick is shit and Input.addListener() doesn't work
 * <p/>
 * Created by fraserkillip on 28/07/14.
 */
public class InputProxy implements KeyListener, MouseListener {

    public List<KeyListener> keyListeners = new ArrayList<KeyListener>();
    public List<MouseListener> mouseListeners = new ArrayList<MouseListener>();

    public List<KeyListener> getKeyListeners() {
        return keyListeners;
    }

    public void addKeyListener(KeyListener keyListener) {
        keyListeners.add(keyListener);
    }

    public void removeKeyListener(KeyListener keyListener) {
        keyListeners.remove(keyListener);
    }

    public List<MouseListener> getMouseListeners() {
        return mouseListeners;
    }

    public void addMouseListener(MouseListener mouseListener) {
        mouseListeners.add(mouseListener);
    }

    public void removeMouseListener(MouseListener mouseListener) {
        mouseListeners.remove(mouseListener);
    }


    @Override
    public void keyPressed(int i, char c) {
        for (KeyListener listener : keyListeners) listener.keyPressed(i, c);
    }

    @Override
    public void keyReleased(int i, char c) {
        for (KeyListener listener : keyListeners) listener.keyReleased(i, c);
    }

    @Override
    public void mouseWheelMoved(int i) {
        for (MouseListener listener : mouseListeners) listener.mouseWheelMoved(i);
    }

    @Override
    public void mouseClicked(int i, int i2, int i3, int i4) {
        for (MouseListener listener : mouseListeners) listener.mouseClicked(i, i2, i3, i4);
    }

    @Override
    public void mousePressed(int i, int i2, int i3) {
        for (MouseListener listener : mouseListeners) listener.mousePressed(i, i2, i3);
    }

    @Override
    public void mouseReleased(int i, int i2, int i3) {
        for (MouseListener listener : mouseListeners) listener.mouseReleased(i, i2, i3);
    }

    @Override
    public void mouseMoved(int i, int i2, int i3, int i4) {
        for (MouseListener listener : mouseListeners) listener.mouseMoved(i, i2, i3, i4);
    }

    @Override
    public void mouseDragged(int i, int i2, int i3, int i4) {
        for (MouseListener listener : mouseListeners) listener.mouseDragged(i, i2, i3, i4);
    }

    @Override
    public void setInput(Input input) {
    }

    @Override
    public boolean isAcceptingInput() {
        return true;
    }

    @Override
    public void inputEnded() {
    }

    @Override
    public void inputStarted() {
    }
}
