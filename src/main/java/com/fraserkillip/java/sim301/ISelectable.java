package com.fraserkillip.java.sim301;

/**
 * Created by fraserkillip on 28/07/14.
 */
public interface ISelectable {

    public boolean isSelected();

    public void setSelected(boolean selected);

    public boolean canSelect();

    public boolean isMouseOver();
}
