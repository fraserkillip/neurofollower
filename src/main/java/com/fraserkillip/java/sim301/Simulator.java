package com.fraserkillip.java.sim301;

import com.fraserkillip.java.sim301.states.MainState;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Created by fraserkillip on 24/07/14.
 */
public class Simulator extends StateBasedGame {

    /* InputProxy to tunnel input events to the objects that need it */
    public static InputProxy inputProxy = new InputProxy();

    public Simulator(String name) {
        super(name);
    }


    @Override
    public void initStatesList(GameContainer gameContainer) throws SlickException {
        addState(new MainState());
    }


    @Override
    public void keyReleased(int key, char c) {
        super.keyReleased(key, c);
        inputProxy.keyReleased(key, c);
    }

    @Override
    public void keyPressed(int key, char c) {
        super.keyPressed(key, c);
        inputProxy.keyPressed(key, c);
    }

    @Override
    public void mouseClicked(int button, int x, int y, int clickCount) {
        super.mouseClicked(button, x, y, clickCount);
        inputProxy.mouseClicked(button, x, y, clickCount);
    }

    @Override
    public void mouseDragged(int oldx, int oldy, int newx, int newy) {
        super.mouseDragged(oldx, oldy, newx, newy);
        inputProxy.mouseDragged(oldx, oldy, newx, newy);
    }

    @Override
    public void mouseMoved(int oldx, int oldy, int newx, int newy) {
        super.mouseMoved(oldx, oldy, newx, newy);
        inputProxy.mouseMoved(oldx, oldy, newx, newy);
    }

    @Override
    public void mousePressed(int button, int x, int y) {
        super.mousePressed(button, x, y);
        inputProxy.mousePressed(button, x, y);
    }

    @Override
    public void mouseReleased(int button, int x, int y) {
        super.mouseReleased(button, x, y);
        inputProxy.mouseReleased(button, x, y);
    }

    @Override
    public void mouseWheelMoved(int newValue) {
        super.mouseWheelMoved(newValue);
        inputProxy.mouseWheelMoved(newValue);
    }
}