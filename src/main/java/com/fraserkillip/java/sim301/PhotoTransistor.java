package com.fraserkillip.java.sim301;

import com.fraserkillip.java.sim301.common.ITickable;
import com.fraserkillip.java.sim301.graphics.IRenderable;
import com.fraserkillip.java.sim301.graphics.helper.GraphicsHelper;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.KeyListener;

/**
 * Created by fraserkillip on 27/07/14.
 */
public class PhotoTransistor implements ITickable, IRenderable, ISelectable, KeyListener {

    private int x, y;

    private int width = 30;
    private int height = 30;

    private char brightness = 0;

    private boolean selected = false;

    @Override
    public void draw(Graphics g) {
        GraphicsHelper.pushState(g);

        /* Fill */
        g.setColor(new Color(255, 255, 0, brightness));
        g.fillRect(x, y, width, height);

        /* Frame */
        if (isMouseOver()) {
            g.setColor(Color.red);
            g.drawRect(x - 1, y - 1, width + 2, height + 2);
        }
        if (selected) {
            g.setColor(Color.green);
        } else {
            g.setColor(Color.black);
        }
        g.drawRect(x, y, width, height);

        GraphicsHelper.popState(g);
    }

    @Override
    public void tick(float delta) {

    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public char getBrightness() {
        return brightness;
    }

    public void setBrightness(char brightness) {
        this.brightness = brightness;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override
    public boolean canSelect() {
        return true;
    }

    @Override
    public boolean isMouseOver() {
        return InputHelper.isMouseInBounds(x, y, width, height);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public void keyPressed(int i, char c) {

    }

    @Override
    public void keyReleased(int i, char c) {

    }

    @Override
    public void setInput(Input input) {
    }

    @Override
    public boolean isAcceptingInput() {
        return false;
    }

    @Override
    public void inputEnded() {
    }

    @Override
    public void inputStarted() {
    }
}
