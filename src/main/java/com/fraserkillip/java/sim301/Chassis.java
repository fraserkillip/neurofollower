package com.fraserkillip.java.sim301;

import com.fraserkillip.java.sim301.common.ITickable;
import com.fraserkillip.java.sim301.graphics.IRenderable;
import com.fraserkillip.java.sim301.graphics.helper.GraphicsHelper;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fraserkillip on 27/07/14.
 */
public class Chassis implements IRenderable, ITickable, KeyListener, MouseListener {

    private int x, y, rotation;

    private Shape bodyShape;

    private Shape topPoint, leftPoint, rightPoint;
    private List<Shape> sensors = new ArrayList<Shape>();

    public Chassis(int x, int y, int rotation) {
        this.x = x;
        this.y = y;
        this.rotation = rotation;

        float[] points = new float[]{0.0f, 0.0f, 1.0f, 3.0f, -1.0f, 3.0f};

        bodyShape = new Polygon(points);

        bodyShape = bodyShape.transform(Transform.createScaleTransform(10, 10));

        topPoint = new Point(0.0f,0.0f);
        leftPoint = new Point(1.0f,3.0f);
        rightPoint = new Point(-1.0f,3.0f);

        sensors.add(new Rectangle(0, -1, 2, 2));

        this.transform(Transform.createTranslateTransform(100, 100));

    }

    @Override
    public void draw(Graphics g) {
        GraphicsHelper.pushState(g);

        g.setColor(Color.blue);
        g.drawRect(x, y, 50, 50);

        for (Shape shape : sensors) {
            g.draw(shape);
        }

        g.draw(bodyShape);

        g.setColor(Color.red);
        g.setLineWidth(10);


        g.fillOval(topPoint.getCenterX(), topPoint.getCenterY(), 2, 2);
        g.fillOval(leftPoint.getCenterX(), leftPoint.getCenterY(), 2, 2);
        g.fillOval(rightPoint.getCenterX(), rightPoint.getCenterY(), 2, 2);

        GraphicsHelper.popState(g);
    }

    @Override
    public void tick(float delta) {

        topPoint = new Point(bodyShape.getPoint(0)[0],bodyShape.getPoint(0)[1]);
        leftPoint = new Point(bodyShape.getPoint(1)[0],bodyShape.getPoint(1)[1]);
        rightPoint = new Point(bodyShape.getPoint(2)[0],bodyShape.getPoint(2)[1]);

        float rotationPointX = (leftPoint.getCenterX() + rightPoint.getCenterX())/2;
        float rotationPointY = (leftPoint.getCenterY() + rightPoint.getCenterY())/2;

        Transform transform = Transform.createRotateTransform(0.001f, rotationPointX, rotationPointY);

        bodyShape = bodyShape.transform(transform);

        List<Shape> sensorsTemp = new ArrayList<Shape>();

        for (Shape shape : sensors) {
            sensorsTemp.add(shape.transform(transform));
        }

        sensors = sensorsTemp;

    }

    public void transform(Transform transform) {
        bodyShape = bodyShape.transform(transform);

        List<Shape> sensorsTemp = new ArrayList<Shape>();

        for (Shape shape : sensors) {
            sensorsTemp.add(shape.transform(transform));
        }

        sensors = sensorsTemp;

    }

    @Override
    public void keyPressed(int i, char c) {
        System.out.println(i);
    }

    @Override
    public void keyReleased(int i, char c) {

    }

    @Override
    public void mouseWheelMoved(int i) {

    }

    @Override
    public void mouseClicked(int i, int i2, int i3, int i4) {

    }

    @Override
    public void mousePressed(int i, int i2, int i3) {

    }

    @Override
    public void mouseReleased(int i, int i2, int i3) {
        System.out.println("Mouse clicked");
    }

    @Override
    public void mouseMoved(int i, int i2, int i3, int i4) {

    }

    @Override
    public void mouseDragged(int i, int i2, int i3, int i4) {

    }

    @Override
    public void setInput(Input input) {

    }

    @Override
    public boolean isAcceptingInput() {
        return false;
    }

    @Override
    public void inputEnded() {

    }

    @Override
    public void inputStarted() {

    }
}
