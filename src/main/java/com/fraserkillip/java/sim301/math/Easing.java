package com.fraserkillip.java.sim301.math;

/**
 * Allows the a linear time to be mapped to a non-linear easing
 * <p/>
 * Adapted with license from WebKit timing functions
 *
 * @author Webkit
 */
public class Easing {

    // ---- Linear ----

    public static final Easing linear = new Easing(0F, 0F, 1F, 1F);

    // ---- Sine -----

    public static final Easing easeInSine = new Easing(0.47F, 0F, 0.745F, 0.715F);

    public static final Easing easeOutSine = new Easing(0.39F, 0.575F, 0.565F, 1F);

    public static final Easing easeInOutSine = new Easing(0.445F, 0.05F, 0.55F, 0.95F);

    // ---- Quad -----

    public static final Easing easeInQuad = new Easing(0.55F, 0.085F, 0.68F, 0.53F);

    public static final Easing easeOutQuad = new Easing(0.25F, 0.46F, 0.45F, 0.94F);

    public static final Easing easeInOutQuad = new Easing(0.455F, 0.03F, 0.515F, 0.955F);

    // ---- Cubic -----
    public static final Easing easeInCubic = new Easing(0.55F, 0.055F, 0.675F, 0.19F);

    public static final Easing easeOutCubic = new Easing(0.215F, 0.61F, 0.355F, 1F);

    public static final Easing easeInOutCubic = new Easing(0.645F, 0.045F, 0.355F, 1F);


    // ---- Class Core ----
    private float ax = 0, bx = 0, cx = 0, ay = 0, by = 0, cy = 0;

    // For examples; see http://cubic-bezier.com
    public Easing(float x1, float y1, float x2, float y2) {
        cx = (float) (3.0 * x1);
        bx = (float) (3.0 * (x2 - x1) - cx);
        ax = (float) (1.0 - cx - bx);
        cy = (float) (3.0 * y1);
        by = (float) (3.0 * (y2 - y1) - cy);
        ay = (float) (1.0 - cy - by);
    }

    public float getValue(float t) {
        return solve(t, solveEpsilon(1.0f));
    }

    private float sampleCurveX(float t) {
        return ((ax * t + bx) * t + cx) * t;
    }

    private float sampleCurveY(float t) {
        return ((ay * t + by) * t + cy) * t;
    }

    private float sampleCurveDerivativeX(float t) {
        return (float) ((3.0 * ax * t + 2.0 * bx) * t + cx);
    }

    private float solve(float x, float epsilon) {
        return sampleCurveY(solveCurveX(x, epsilon));
    }

    private float solveCurveX(float x, float epsilon) {
        float t0, t1, t2, x2, d2, i;
        // First try a few iterations of Newton's method -- normally very fast.
        for (t2 = x, i = 0; i < 8; i++) {
            x2 = sampleCurveX(t2) - x;
            if (fabs(x2) < epsilon) {
                return t2;
            }
            d2 = sampleCurveDerivativeX(t2);
            if (fabs(d2) < 1e-6) {
                break;
            }
            t2 = t2 - x2 / d2;
        }

        // Fall back to the bisection method for reliability.
        t0 = 0.0F;
        t1 = 1.0F;
        t2 = x;
        if (t2 < t0) {
            return t0;
        }
        if (t2 > t1) {
            return t1;
        }
        while (t0 < t1) {
            x2 = sampleCurveX(t2);
            if (fabs(x2 - x) < epsilon) {
                return t2;
            }
            if (x > x2) {
                t0 = t2;
            } else {
                t1 = t2;
            }
            t2 = (float) ((t1 - t0) * .5 + t0);
        }
        return t2; // Failure.
    }

    private float fabs(float n) {
        if (n >= 0) {
            return n;
        } else {
            return 0 - n;
        }
    }

    private float solveEpsilon(float duration) {
        return (float) (1.0 / (200.0 * duration));
    }

}
