package com.fraserkillip.java.sim301;

import org.newdawn.slick.AppGameContainer;

import java.io.File;

/**
 * Created by fraserkillip on 24/07/14.
 */
public class launcher {
    private static final String OS = System.getProperty("os.name").toLowerCase();

    public static void main(String[] args) throws Exception {
        // Set natives path
        String nativesFolder = "";
        if (OS.indexOf("win") >= 0) {
            nativesFolder = "\\windows";
        } else if (OS.indexOf("mac") >= 0) {
            nativesFolder = "/mac";
        } else if (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0) {
            nativesFolder = "/linux";
        } else {
            throw new Exception("OS not recognised, cannot load natives");
        }
        System.setProperty("org.lwjgl.librarypath", new File("natives" + nativesFolder).getAbsolutePath());

        // Create a new game container
        AppGameContainer game = new AppGameContainer(new Simulator(GameInfo.GAME_TITLE));

        // Set game size
        game.setDisplayMode(GameInfo.GAME_WIDTH, GameInfo.GAME_HEIGHT, false);

        // Set target FPS
//        game.setTargetFrameRate(60);

        // Set VSync to prevent tearing
        game.setVSync(true);

        // Set update rate
        game.setMaximumLogicUpdateInterval(1);
        game.setMinimumLogicUpdateInterval(1);

        // Make sure FPS is hidden by default
        game.setShowFPS(true);

        game.setUpdateOnlyWhenVisible(false);

        // Start the game
        game.start();
    }
}
