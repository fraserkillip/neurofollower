package com.fraserkillip.java.sim301.graphics.gui;

import com.fraserkillip.java.sim301.graphics.IRenderable;
import com.fraserkillip.java.sim301.graphics.helper.GraphicsHelper;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.gui.GUIContext;

public class Button extends BaseComponent implements IRenderable {
    protected GUIContext container;

    private boolean disabled;

    private ButtonListener listener = null;

    private boolean mouseWasOver = false;

    public Button(GUIContext container, int id, String message, int x, int y, int width, int height) {
        super(container, id, message, x, y, width, height);

        this.message = message;

        this.disabled = false;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public boolean isDisabled() {
        return disabled;
    }

    @Override
    public void mouseMoved(int oldx, int oldy, int newx, int newy) {
        // TODO Auto-generated method stub
        super.mouseMoved(oldx, oldy, newx, newy);

        if (isMouseOver()) {
            if (!mouseWasOver) {
                mouseWasOver = true;
            }
        } else {
            mouseWasOver = false;
        }
    }

    @Override
    public void mousePressed(int button, int x, int y) {
        if (isMouseOver()) {
            if (listener != null && !isDisabled()) {
                listener.buttonPressed(id);
            }
        }
    }

    @Override
    public void draw(Graphics g) {
        // x and y will always be zero here
        GraphicsHelper.pushState(g);

        if (currentFont == null) setDefaultFont();

        g.setFont(currentFont);

        // If the font still isn't loaded
        if (currentFont == null) return;

        int stringX = getX() + getWidth() / 2 - currentFont.getWidth(message) / 2;
        int stringY = getY() + getHeight() / 2 - currentFont.getHeight(message) / 2;

        boolean mouseIsOver = isMouseOver();

        // Draw the base
        g.setColor(Color.black);
        g.fillRect(getX(), getY(), getWidth(), getHeight());

        // Draw main background
        if (!isDisabled()) {
            g.setColor(new Color(0xFFCCCCCC));
            g.fillRect(getX() + 2, getY() + 2, getWidth() - 4, getHeight() - 4);

            g.setColor(new Color(0x55FFFFFF));
            g.fillRect(getX() + 2, getY() + 2, getWidth() - 4, 3); // Top bar
            g.fillRect(getX() + 2, getY() + 5, 3, getHeight() - 10); // Left Bar

            // Draw bottom bevel
            g.setColor(new Color(0x55000000));
            g.fillRect(getX() + 2, getY() + getHeight() - 4, getWidth() - 4, 2); // Bottom
            // Bar
            g.fillRect(getX() + getWidth() - 4, getY() + 5, 2, getHeight() - 9); // Right
            // bar

            if (mouseIsOver) {
                g.setColor(new Color(0x66929CD0));
                g.fillRect(getX() + 2, getY() + 2, getWidth() - 4, getHeight() - 4);
            }

            // Draw text shadow
            g.setColor(new Color(0x66000000));
            g.drawString(message, stringX + 2, stringY + 2);

            // Draw text
            if (mouseIsOver) {
                g.setColor(new Color(0xFFFFE166));
            } else {
                g.setColor(Color.white);
            }
            g.drawString(message, stringX, stringY);

        } else {
            g.setColor(new Color(0xFF444444));
            g.fillRect(getX() + 2, getY() + 2, getWidth() - 4, getHeight() - 4);

            g.setColor(new Color(0xBBFFFFFF));
            g.drawString(message, stringX, stringY);
        }

        GraphicsHelper.popState(g);
    }

    public void setListener(ButtonListener listener) {
        this.listener = listener;
    }

    @Override
    public <T> T dispose() {
        this.listener = null;
        this.setAcceptingInput(false);
        return null;
    }
}
