package com.fraserkillip.java.sim301.graphics.gui;

import com.fraserkillip.java.sim301.graphics.IRenderable;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;


public class ProgressBar implements IRenderable {

    private int x, y;

    private float width = 100;
    private float progress = 0;

    public ProgressBar(int x, int y, float width, float progress) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.progress = progress;
    }

    @Override
    public void draw(Graphics g) {
        // TODO Auto-generated method stub

        g.setColor(Color.darkGray);
        g.fillRoundRect(x, y, width, 20, 10);

        g.setColor(Color.cyan);
        g.fillRoundRect(x, y, width * progress, 20, 10);

    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
