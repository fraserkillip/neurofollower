package com.fraserkillip.java.sim301.graphics.gui;

import com.fraserkillip.java.sim301.graphics.IFadeable;
import com.fraserkillip.java.sim301.graphics.helper.GraphicsHelper;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.gui.GUIContext;

/**
 * Defines a string of text to be drawn on screen
 *
 * @author fraserkillip
 */
public class TextField extends BaseComponent implements IFadeable {

    private Color color;

    /**
     * @param container GameContainer of the State
     * @param id        Local ID used to identify interactions with the control
     * @param message   The text to be displayed
     * @param x         The absolute x coordinate
     * @param y         The absolute y coordinate
     * @param width     The width
     * @param height    The height
     * @param font      The font to use
     * @param color     The color to use
     */
    public TextField(GUIContext container, int id, String message, int x, int y, int width, int height, Font font, Color color) {
        super(container, id, message, x, y, width, height);

        this.currentFont = font;

        this.color = color.scaleCopy(1.0f);
    }

    @Override
    public void draw(Graphics g) {
        GraphicsHelper.pushState(g);

        g.setFont(currentFont);
        g.setColor(color);
        g.drawString(getMessage(), getX(), getY());

        GraphicsHelper.popState(g);
    }

    @Override
    public float getOpacity() {
        return color.a;
    }

    @Override
    public void setOpacity(float opacity) {
        color.a = opacity;
    }

    @Override
    public <T> T dispose() {
        return null;
    }

}
