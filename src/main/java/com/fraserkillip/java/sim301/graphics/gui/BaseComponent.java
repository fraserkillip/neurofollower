package com.fraserkillip.java.sim301.graphics.gui;

import com.fraserkillip.java.sim301.GameInfo;
import com.fraserkillip.java.sim301.graphics.ILocationRenderable;
import com.fraserkillip.java.sim301.graphics.font.MainFont;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.gui.AbstractComponent;
import org.newdawn.slick.gui.GUIContext;

public abstract class BaseComponent extends AbstractComponent implements ILocationRenderable {

    protected int id;

    protected int height;
    protected int width;

    protected int xPos;
    protected int yPos;

    protected Font currentFont = null;

    protected String message;

    public BaseComponent(GUIContext container, int id, String message, int x, int y, int width, int height) {
        super(container);

        this.container = container;

        this.id = id;

        this.xPos = x;
        this.yPos = y;

        this.height = height;
        this.width = width;

        this.message = message;
    }

    public int id() {
        return id;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getWidth() {
        return width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    @Override
    public int getX() {
        return xPos;
    }

    @Override
    public int getY() {
        return yPos;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void render(GUIContext container, Graphics g) throws SlickException {
        draw(g);
    }

    @Override
    public void setLocation(int x, int y) {
        this.xPos = x;
        this.yPos = y;
    }

    @Override
    public void setLocation(Vector2f location) {
        this.xPos = (int) location.x;
        this.yPos = (int) location.y;
    }

    @Override
    public Vector2f getLocation() {
        // TODO Auto-generated method stub
        return new Vector2f(xPos, yPos);
    }

    public void setFont(Font font) {
        currentFont = font;
    }

    public void setDefaultFont() {
        currentFont = MainFont.font20;
    }

    public boolean isMouseOver() {
        int mouseX = Mouse.getX();
        int mouseY = GameInfo.GAME_HEIGHT - Mouse.getY(); // Mouse.getY() is
        // from the bottom
        return mouseX > this.getX() && mouseX < this.getX() + this.getWidth() && mouseY > this.getY() && mouseY < this.getY() + this.getHeight();
    }

    public abstract <T> T dispose();
}
