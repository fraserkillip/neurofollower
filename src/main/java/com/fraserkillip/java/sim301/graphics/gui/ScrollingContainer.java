package com.fraserkillip.java.sim301.graphics.gui;

import com.fraserkillip.java.sim301.graphics.helper.GraphicsHelper;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.gui.GUIContext;

public class ScrollingContainer extends GuiContainer implements SliderListener {

    private Slider slider;

    private int prevSliderVal = 0;

    public ScrollingContainer(GUIContext container, int id, int x, int y, int width, int height, int scrollWidth) {
        super(container, id, x, y, width, height);

        slider = new Slider(container, id, x, y + height - 30, width, 30, Math.max(scrollWidth - width, 0), 0);
        slider.setListener(this);
    }

    @Override
    public void draw(Graphics g) {
        GraphicsHelper.pushState(g);

        g.setColor(new Color(0f, 0f, 0f, 0.4f));
        g.fillRect(getX(), getY(), getWidth(), getHeight());

        super.draw(g);

        GraphicsHelper.popState(g);

        slider.draw(g);
    }

    @Override
    public void sliderValueUpdated(int id) {
        int delta = slider.getValue() - prevSliderVal;

        prevSliderVal = slider.getValue();

        for (BaseComponent comp : components) {
            comp.setLocation(comp.getLocation().sub(new Vector2f(delta, 0f)));
        }
    }


}
