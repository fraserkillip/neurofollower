package com.fraserkillip.java.sim301.graphics.gui;

public interface ButtonListener {
    public void buttonPressed(int id);
}
