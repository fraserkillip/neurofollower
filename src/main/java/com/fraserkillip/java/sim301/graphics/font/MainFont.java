package com.fraserkillip.java.sim301.graphics.font;

import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.loading.LoadingList;

/**
 * Main Font BD Cartoon Shout
 *
 * @author fraserkillip
 */
public class MainFont {
    public static AngelCodeFont font20;
    public static AngelCodeFont font25;
    public static AngelCodeFont font30;
    public static AngelCodeFont font35;
    public static AngelCodeFont font40;

    private static boolean initialised;

    public static void initFonts() throws SlickException {
        // Make sure we don't load it twice accidentally
        if (initialised)
            return;

        initialised = true;

        // Stop deferred loading for fonts
        LoadingList.setDeferredLoading(false);
        MainFont.font20 = new AngelCodeFont("assets/fonts/font20.fnt", "assets/fonts/font20_0.png");
        MainFont.font25 = new AngelCodeFont("assets/fonts/font25.fnt", "assets/fonts/font25_0.png");
        MainFont.font30 = new AngelCodeFont("assets/fonts/font30.fnt", "assets/fonts/font30_0.png");
        MainFont.font35 = new AngelCodeFont("assets/fonts/font35.fnt", "assets/fonts/font35_0.png");
        MainFont.font40 = new AngelCodeFont("assets/fonts/font40.fnt", "assets/fonts/font40_0.png");
        LoadingList.setDeferredLoading(true);
    }
}
