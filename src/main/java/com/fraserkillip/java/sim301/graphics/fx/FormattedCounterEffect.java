package com.fraserkillip.java.sim301.graphics.fx;

import com.fraserkillip.java.sim301.graphics.gui.TextField;
import org.newdawn.slick.Graphics;

import java.util.ArrayList;

public class FormattedCounterEffect extends Effect {

    public static class CounterSet {
        protected int start;
        protected int finish;
        protected float period;

        public CounterSet(int start, int finish, float period) {
            this.start = start;
            this.finish = finish;
            this.period = period;
        }
    }

    private String template;
    private TextField field;
    private ArrayList<CounterSet> sets;

    private float currentTime = 0;

    public FormattedCounterEffect(String template, TextField field, CounterSet... sets) {
        this.template = template;

        this.field = field;

        this.sets = new ArrayList<CounterSet>();

        for (CounterSet set : sets) {
            this.sets.add(set);
        }
    }

    @Override
    public void draw(Graphics g) {
        field.draw(g);
    }

    @Override
    public void tick(float delta) {
        if (!isStarted()) return;

        currentTime += delta;

        Object[] values = new Object[sets.size()];

        boolean allFinished = true;

        for (int i = 0; i < sets.size(); i++) {
            CounterSet current = sets.get(i);
            boolean finished = current.period < currentTime;
            if (!finished) {
                values[i] = (int) ((current.finish - current.start) * getTimingFunction().getValue(currentTime / current.period));
            } else {
                values[i] = current.finish;
            }
            allFinished &= finished;
        }

        field.setMessage(String.format(template, values));

        if (allFinished) {
            didFinish();
        }
    }

    @Override
    public void complete(boolean stopChain) {
        // TODO Auto-generated method stub

    }

    @Override
    public void stop(boolean stopChain) {
        // TODO Auto-generated method stub

    }

}
