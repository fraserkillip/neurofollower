package com.fraserkillip.java.sim301.graphics.gui;

import org.newdawn.slick.gui.GUIContext;

public class RadioButton extends Button {

    private RadioGroup group;

    protected RadioButton(GUIContext container, int id, String message, int x, int y, int width, int height, RadioGroup group) {
        super(container, id, message, x, y, width, height);
        this.group = group;
    }

    @Override
    public void mousePressed(int button, int x, int y) {
        super.mousePressed(button, x, y);
        if (isMouseOver()) {
            group.check(this);
        }
    }

}
