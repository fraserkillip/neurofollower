package com.fraserkillip.java.sim301.graphics.fx;

import com.fraserkillip.java.sim301.common.ITickable;
import com.fraserkillip.java.sim301.graphics.IRenderable;
import com.fraserkillip.java.sim301.math.Easing;

import java.util.ArrayList;

public abstract class Effect implements IRenderable, ITickable {

    private ArrayList<Effect> queued;

    private boolean isStarted = false;

    private boolean isPaused = false;

    private boolean isFinished = false;

    private Easing timingFunction = Easing.linear;

    private boolean doDraw = true;

    public Effect() {
        queued = new ArrayList<Effect>();
    }

    public abstract void complete(boolean stopChain);

    public abstract void stop(boolean stopChain);

    public void queueEffect(Effect effect) {
        queued.add(effect);
    }

    public void dequeueEffect(Effect effect) {
        queued.remove(effect);
    }

    public boolean isQueued(Effect effect) {
        return queued.contains(effect);
    }

    public ArrayList<Effect> getQueued() {
        return queued;
    }

    public boolean isStarted() {
        return isStarted;
    }

    public void start() {
        isStarted = true;
        isPaused = false;
    }

    public boolean isPaused() {
        return isPaused;
    }

    public void pause() {
        isPaused = true;
        isStarted = false;
    }

    public boolean isFinished(boolean checkQueued) {
        if (!checkQueued) return isFinished;

        for (Effect effect : queued) {
            if (!effect.isFinished(checkQueued)) {
                return false;
            }
        }
        return isFinished;
    }

    public void setFinished(boolean isFinished) {
        this.isFinished = isFinished;
        if (isFinished) {
            isStarted = false;
            isPaused = false;
        }
    }

    protected final void didFinish() {
        if (isFinished(false)) return;
        setFinished(true);
        for (Effect effect : queued) {
            effect.start();
        }
    }

    public Easing getTimingFunction() {
        return timingFunction;
    }

    public void setTimingFunction(Easing timingFunction) {
        this.timingFunction = timingFunction;
    }

    public boolean doDraw() {
        return doDraw;
    }

    public void setDraw(boolean doDraw) {
        this.doDraw = doDraw;
    }

}
