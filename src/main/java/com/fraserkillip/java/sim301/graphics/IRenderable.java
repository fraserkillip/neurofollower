package com.fraserkillip.java.sim301.graphics;

import org.newdawn.slick.Graphics;

/**
 * Defines a method to render the object to a Slick graphics canvas
 *
 * @author fraserkillip
 */
public interface IRenderable {

    public void draw(Graphics g);
}
