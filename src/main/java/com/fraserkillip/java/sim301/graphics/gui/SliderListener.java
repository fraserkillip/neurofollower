package com.fraserkillip.java.sim301.graphics.gui;

/**
 * Defines interface for receiving updates from a Slider
 *
 * @author fraserkillip
 */
public interface SliderListener {
    public void sliderValueUpdated(int id);
}
