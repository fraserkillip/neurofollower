package com.fraserkillip.java.sim301.graphics.fx;

import com.fraserkillip.java.sim301.graphics.IRenderable;
import com.fraserkillip.java.sim301.graphics.helper.GraphicsHelper;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;

public class PulsingEffect extends Effect {

    private IRenderable pulser;

    private float initialScale;

    private float finalScale;

    private int cycles;

    private float currentCycle = 0;

    private float scaleDelta;

    private float currentScale;

    private boolean ascending = true;

    private Vector2f center;

    Vector2f scaledCenter = new Vector2f();

    private Vector2f translation;

    // Period in milliseconds
    public PulsingEffect(IRenderable pulser, float initialScale, float finalScale, float period, int cycles, Vector2f center) {
        this.pulser = pulser;

        this.initialScale = initialScale;

        this.finalScale = finalScale;

        this.scaleDelta = (finalScale - initialScale) / (period / 2);

        this.cycles = cycles;

        this.currentScale = initialScale;

        this.center = center;

        this.translation = new Vector2f();
    }

    @Override
    public void draw(Graphics g) {
        GraphicsHelper.pushState(g);

        g.translate(translation.x, translation.y);
        g.scale(currentScale, currentScale);

        pulser.draw(g);

        GraphicsHelper.popState(g);
    }

    @Override
    public void tick(float delta) {
        if (!isStarted()) return;

        currentScale += (ascending ? 1 : -1) * scaleDelta;

        boolean ascendingBefore = ascending;

        if (initialScale < finalScale) {
            if (currentScale >= finalScale) {
                ascending = false;
            } else if (currentScale <= initialScale) {
                ascending = true;
            }
        } else {
            if (currentScale >= initialScale) {
                ascending = true;
            } else if (currentScale <= finalScale) {
                ascending = false;
            }
        }

        if (ascendingBefore != ascending) {
            currentCycle += 0.5;
        }

        if (cycles != -1 && currentCycle >= cycles) {
            stop(false);
        }

        scaledCenter = center.copy().scale(currentScale);

        translation = center.copy().sub(scaledCenter);
    }

    @Override
    public void start() {
        super.start();

        this.currentCycle = 0;
    }

    @Override
    public void complete(boolean stopChain) {
        // TODO Auto-generated method stub

    }

    @Override
    public void stop(boolean stopChain) {
        this.setFinished(true);
        didFinish();
    }

}
