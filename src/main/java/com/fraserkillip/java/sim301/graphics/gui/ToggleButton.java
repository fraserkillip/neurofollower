package com.fraserkillip.java.sim301.graphics.gui;

import org.newdawn.slick.gui.GUIContext;

import java.util.List;

/**
 * A button that can have it's state toggled
 *
 * @author fraserkillip
 */
public class ToggleButton extends Button {

    private List<String> items;

    private int index = 0;

    public ToggleButton(GUIContext container, int id, List<String> items, int x, int y, int width, int height) {
        super(container, id, items.get(0), x, y, width, height);
        this.items = items;
    }

    @Override
    public void mouseReleased(int button, int x, int y) {
        super.mouseReleased(button, x, y);

        if (isMouseOver()) {
            index++;
            if (index >= items.size()) index = 0;
            message = items.get(index);
        }
    }

}
