package com.fraserkillip.java.sim301.graphics.gui;

import com.fraserkillip.java.sim301.graphics.IRenderable;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.gui.GUIContext;

import java.util.ArrayList;
import java.util.List;


public class RadioGroup implements IRenderable {

    private List<RadioButton> buttons;

    public RadioGroup() {
        buttons = new ArrayList<RadioButton>();
    }

    public RadioButton createButton(GUIContext container, int id, String message, int x, int y, int width, int height) {
        RadioButton newButton = new RadioButton(container, id, message, x, y, width, height, this);

        buttons.add(newButton);

        return newButton;
    }

    public void check(RadioButton radioButton) {
        for (RadioButton button : buttons) {
            button.setDisabled(false);
        }

        radioButton.setDisabled(true);
    }

    public void removeButton(RadioButton button) {
        buttons.remove(button);
    }

    @Override
    public void draw(Graphics g) {
        for (RadioButton button : buttons) {
            button.draw(g);
        }
    }

    public int getCheckedId() {
        for (RadioButton button : buttons) {
            if (button.isDisabled()) return button.id();
        }

        return -1;
    }

    public RadioGroup dispose() {
        for (RadioButton button : buttons) {
            button.dispose();
        }

        buttons.clear();

        return null;
    }
}
