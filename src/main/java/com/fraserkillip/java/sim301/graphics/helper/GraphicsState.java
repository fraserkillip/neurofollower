package com.fraserkillip.java.sim301.graphics.helper;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;

/**
 * Util class that allows easy storing and restoring of graphics states
 *
 * @author fraserkillip
 */
public class GraphicsState {

    private Color background;
    private Color color;

    private Rectangle clip;
    private Rectangle worldClip;

    private float lineWidth;

    private Font font;

    // Constructor
    public GraphicsState(Graphics g) {
        background = g.getBackground();
        clip = g.getClip();
        color = g.getColor();
        font = g.getFont();
        lineWidth = g.getLineWidth();
        worldClip = g.getWorldClip();

        g.pushTransform();
    }

    // Reset the passed graphics context
    public void restoreState(Graphics g) {
        g.clearClip();
        g.clearWorldClip();

        g.setClip(clip);
        g.setWorldClip(worldClip);

        g.setFont(font);

        g.setBackground(background);
        g.setColor(color);

        g.setLineWidth(lineWidth);

        g.popTransform();
    }

}
