package com.fraserkillip.java.sim301.graphics.fx;

import com.fraserkillip.java.sim301.graphics.IFadeable;
import com.fraserkillip.java.sim301.graphics.helper.GraphicsHelper;
import org.newdawn.slick.Graphics;


public class FadeEffect extends Effect {

    private IFadeable item;

    private float startOpacity;

    private float finalOpacity;

    private float currentOpacity;

    private float period;

    private float currentTime = 0;

    public FadeEffect(IFadeable item, float startOpacity, float finalOpacity, float period) {
        this.item = item;

        this.startOpacity = startOpacity;
        this.finalOpacity = finalOpacity;

        this.period = period;
    }

    @Override
    public void draw(Graphics g) {
        GraphicsHelper.pushState(g);

        item.draw(g);

        GraphicsHelper.popState(g);
    }

    @Override
    public void tick(float delta) {
        if (!isStarted() && isFinished(false)) return;

        currentTime += delta;

        if (currentTime > period) {
            currentTime = period;
            didFinish();
        }

        float percent = getTimingFunction().getValue(currentTime / period);

        currentOpacity = finalOpacity - startOpacity * percent + startOpacity;

        item.setOpacity(currentOpacity);
    }

    @Override
    public void complete(boolean stopChain) {
        // TODO Auto-generated method stub

    }

    @Override
    public void stop(boolean stopChain) {
        // TODO Auto-generated method stub

    }

}
