package com.fraserkillip.java.sim301.common;

public interface IContainer {

    public int getxPos();

    public int getyPos();

}
