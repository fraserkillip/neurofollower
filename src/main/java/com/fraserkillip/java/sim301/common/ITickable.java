package com.fraserkillip.java.sim301.common;

public interface ITickable {

    public abstract void tick(float delta);

}