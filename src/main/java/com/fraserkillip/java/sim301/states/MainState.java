package com.fraserkillip.java.sim301.states;

import com.fraserkillip.java.sim301.Chassis;
import com.fraserkillip.java.sim301.GameInfo;
import com.fraserkillip.java.sim301.PhotoTransistor;
import com.fraserkillip.java.sim301.Simulator;
import com.fraserkillip.java.sim301.graphics.font.MainFont;
import com.fraserkillip.java.sim301.graphics.gui.Button;
import com.fraserkillip.java.sim301.graphics.gui.Slider;
import com.fraserkillip.java.sim301.graphics.gui.TextField;
import org.newdawn.slick.*;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Created by fraserkillip on 24/07/14.
 */
public class MainState extends BasicGameState {

    /* Play and stop buttons */
    private Button playButton = null, stopButton = null;

    /* Labels */
    private TextField settingsTextField = null;
    private TextField xPosTextField = null;
    private TextField yPosTextField = null;
    private TextField rotationTextField = null;
    private TextField speedTextField = null;

    /* Sliders */
    private Slider xPosSlider = null;
    private Slider yPosSlider = null;
    private Slider rotationSlider = null;
    private Slider speedSlider = null;

    private final int sidePanelWidth = 320;

    private Chassis chassis = null;

    private PhotoTransistor pt1 = null;
    private char pt1b = 0;


    @Override
    public int getID() {
        return 0;
    }

    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {

    }

    @Override
    public void enter(GameContainer container, StateBasedGame game) throws SlickException {
        try {
            MainFont.initFonts();
        } catch (SlickException e) {
            e.printStackTrace();
        }

        playButton = new Button(container, 0, "PLAY", GameInfo.GAME_WIDTH - sidePanelWidth + 30, GameInfo.GAME_HEIGHT - 100, 100, 30);
        stopButton = new Button(container, 0, "STOP", GameInfo.GAME_WIDTH - sidePanelWidth + 160, GameInfo.GAME_HEIGHT - 100, 100, 30);

        settingsTextField = new TextField(container, 0, "settings", GameInfo.GAME_WIDTH - sidePanelWidth + 30, 20, 200, 30, MainFont.font20, Color.black);

        xPosTextField = new TextField(container, 0, "xPos", GameInfo.GAME_WIDTH - sidePanelWidth + 30, 90, 200, 30, MainFont.font20, Color.black);
        xPosSlider = new Slider(container, 0, GameInfo.GAME_WIDTH - sidePanelWidth + 30, 120, 200, 30, 100, 0);

        yPosTextField = new TextField(container, 0, "yPos", GameInfo.GAME_WIDTH - sidePanelWidth + 30, 160, 200, 30, MainFont.font20, Color.black);
        yPosSlider = new Slider(container, 0, GameInfo.GAME_WIDTH - sidePanelWidth + 30, 190, 200, 30, 100, 0);

        rotationTextField = new TextField(container, 0, "rotation", GameInfo.GAME_WIDTH - sidePanelWidth + 30, 230, 200, 30, MainFont.font20, Color.black);
        rotationSlider = new Slider(container, 0, GameInfo.GAME_WIDTH - sidePanelWidth + 30, 260, 200, 30, 100, 0);

        speedTextField = new TextField(container, 0, "speed", GameInfo.GAME_WIDTH - sidePanelWidth + 30, 300, 200, 30, MainFont.font20, Color.black);
        speedSlider = new Slider(container, 0, GameInfo.GAME_WIDTH - sidePanelWidth + 30, 330, 200, 30, 100, 0);

        pt1 = new PhotoTransistor();
        pt1.setX(100);
        pt1.setY(100);

        chassis = new Chassis(200, 200, 300);

        Simulator.inputProxy.addKeyListener(chassis);
        Simulator.inputProxy.addMouseListener(chassis);
    }

    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics g) throws SlickException {
        g.setBackground(Color.white);

        g.setColor(new Color(0, 0, 0, 0.1f));
        g.fillRect(GameInfo.GAME_WIDTH - sidePanelWidth, 0, sidePanelWidth, GameInfo.GAME_HEIGHT);

        playButton.draw(g);
        stopButton.draw(g);

        settingsTextField.draw(g);

        xPosTextField.setMessage("xPos: " + xPosSlider.getValue());
        xPosTextField.draw(g);
        xPosSlider.draw(g);

        yPosTextField.setMessage("yPos: " + yPosSlider.getValue());
        yPosTextField.draw(g);
        yPosSlider.draw(g);

        rotationTextField.setMessage("rotation: " + rotationSlider.getValue());
        rotationTextField.draw(g);
        rotationSlider.draw(g);

        speedTextField.setMessage("speed: " + speedSlider.getValue());
        speedTextField.draw(g);
        speedSlider.draw(g);

        chassis.draw(g);
    }

    @Override
    public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {
        chassis.tick(i);
    }

    @Override
    public void mouseReleased(int button, int x, int y) {
        if (button == Input.MOUSE_LEFT_BUTTON) {
            if (pt1.isMouseOver() && pt1.canSelect()) {
                pt1.setSelected(!pt1.isSelected());
            }
        }
    }
}
